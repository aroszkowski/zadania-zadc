import sys
import apachelog


def parse(filename):
    format = r'%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"'
    parser = apachelog.parser(format)

    with open(filename, 'r') as file:
        for line in file:
            try:
                yield parser.parse(line)
            except:
                sys.stderr.write("Problem z parsowaniem %s" % line)
