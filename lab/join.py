# -*- coding: utf-8 -*-

import json
from MapReduce import MapReduce


def map(item):
    yield(item.pop(1), item)


def reduce(key, values):
    results = list()
    for v in values:
        if v[0] != 'orders':
            results.append([key]+values[0]+v)
    return results

if __name__ == '__main__':
    input_data = list()
    json_file = open('records.json','r')
    for line in json_file:
        input_data.append(json.loads(line))
    mapper = MapReduce(map, reduce)
    results = mapper(input_data)
    for row in results:
        for r in row:
            print json.dumps(r)
